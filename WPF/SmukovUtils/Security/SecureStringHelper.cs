﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace SmukovUtils.Security
{
    /// <summary>
    /// Contains helper methods that can be used when working with SecureString
    /// </summary>
    public class SecureStringHelper
    {
        /// <summary>
        /// Compares if two SecureString instances contain same data
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns>TRUE - they contain same data, FALSE - otherwise</returns>
        public static bool SecureStringEqual(SecureString s1, SecureString s2)
        {
            if (s1 == null)
            {
                return false;
            }
            if (s2 == null)
            {
                return false;
            }

            if (s1.Length != s2.Length)
            {
                return false;
            }

            IntPtr bstr1 = IntPtr.Zero;
            IntPtr bstr2 = IntPtr.Zero;


            byte[] sBytes1 = new byte[s1.Length *2];
            byte[] sBytes2 = new byte[s2.Length *2];

            try
            {
                Marshal.Copy((bstr1 = Marshal.SecureStringToBSTR(s1)),
                    sBytes1, 0, sBytes1.Length);

                Marshal.Copy((bstr2 = Marshal.SecureStringToBSTR(s2)),
                    sBytes2, 0, sBytes2.Length);

                for (int i = 0; i < sBytes1.Length; i++ )
                {
                    if (sBytes1[i] != sBytes2[i])
                        return false;
                }

                return true;
            }
            finally
            {
                if (bstr1 != IntPtr.Zero)
                {
                    Marshal.ZeroFreeBSTR(bstr1);
                }

                if (bstr2 != IntPtr.Zero)
                {
                    Marshal.ZeroFreeBSTR(bstr2);
                }

                //overwrite the bytes to make sure the passwords
                //aren't kept in memory for someone to see
                for (int i = 0; i < sBytes1.Length; i++)
                {
                    sBytes1[i] = 0;
                    sBytes2[i] = 0;
                }
            }
        } 
    }
}
