﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmukovUtils.Security
{
    /// <summary>
    /// Class used to monitor user activity within the specific application, 
    /// and to react to inactivity by calling a delegate Action that can be set within the class.
    /// </summary>
    public class InactivityManager
    {
        #region Fields

        private System.Windows.Forms.Timer _timer = null;

        /// <summary>
        /// Time in minutes after which the inactivity trashold is reached,
        /// and InactivityAction is fired.
        /// </summary>
        private int _inactivityTrashold = 5;

        /// <summary>
        /// InactivityAction to be called after the inactivity trashold has been reached.
        /// </summary>
        private Action _inactivityAction = null;
     
        #endregion

        #region Properties

        /// <summary>
        /// InactivityAction to be called after the inactivity trashold has been reached.
        /// </summary>
        public Action InactivityAction
        {
            get { return _inactivityAction; }
            set { _inactivityAction = value; }
        }

        /// <summary>
        /// Time in minutes after which the inactivity trashold is reached,
        /// and InactivityAction is fired.
        /// </summary>
        public int InactivityTrashold
        {
            get { return _inactivityTrashold; }
            set 
            { 
                _inactivityTrashold = value;

                this._timer.Enabled = false;
                this._timer.Interval = this._inactivityTrashold * 60 * 1000;
                this._timer.Enabled = true;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes the InactivityManager class
        /// </summary>
        /// <param name="inactivityTrashold">Time in minutes after which the inactivity trashold is reached and InactivityAction is fired.</param>
        public InactivityManager(int inactivityTrashold)
        {
            this._inactivityTrashold = inactivityTrashold;

            this._timer = new System.Windows.Forms.Timer();
            this._timer.Interval = this._inactivityTrashold * 60 * 1000;//turn minutes into milliseconds
            this._timer.Tick += _timer_Tick;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Starts the inactivity timer.
        /// </summary>
        public void StartInactivityTimer()
        {
            _timer.Enabled = true;
        }

        /// <summary>
        /// Call this method to report an activity and reset the inactivity timer.
        /// </summary>
        public void ResetInactivityTimer()
        {
            if(_timer != null)
            {
                _timer.Enabled = false;
                _timer.Enabled = true;
            }
        }

        public void StopInactivityTimer()
        {
            _timer.Enabled = false;
        }

        #endregion

        #region Private Methods

        void _timer_Tick(object sender, EventArgs e)
        {
            _timer.Enabled = false;

            if (this._inactivityAction != null)
                _inactivityAction();
        }

        #endregion
    }
}
