﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace SmukovUtils
{
    public class Internet
    {
        /// <summary>
        /// Get computer INTERNET address like 93.136.91.7
        /// </summary>
        /// <returns></returns>
        public string GetCurrentInternetIPAddress()
        {
            string url = "http://checkip.dyndns.org";

            WebRequest req = WebRequest.Create(url);
            try
            {
                using (WebResponse resp = req.GetResponse())
                {
                    using (StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream()))
                    {
                        string response = sr.ReadToEnd().Trim();
                        string[] a = response.Split(':');
                        string a2 = a[1].Substring(1);
                        string[] a3 = a2.Split('<');
                        string a4 = a3[0];
                        return a4;
                    }
                }
            }
            catch (Exception ex)
            {
                return "0.0.0.0";
            }
        }

        /// <summary>
        /// Returns current IP Address in its byte array form
        /// </summary>
        /// <returns></returns>
        public byte[] GetCurrentInternetIPAddressAsByteArray()
        {
            byte[] returnVal = new byte[4];

            string ipInText = this.GetCurrentInternetIPAddress();

            returnVal = ipInText.Split('.').Select(x => byte.Parse(x)).ToArray();

            return returnVal;
        }


    }
}
