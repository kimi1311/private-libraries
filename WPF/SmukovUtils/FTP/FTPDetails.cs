﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmukovUtils.FTP
{
    public class FTPDetails
    {
        private string _rootPath = "";

        public string RootPath
        {
            get { return _rootPath; }
            set { _rootPath = value; }
        }
        private string _username = "";

        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }
        private string _password = "";

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        private bool _useSecure = true;

        public bool UseSecure
        {
            get { return _useSecure; }
            set { _useSecure = value; }
        }
        private string _FTPHost = "";

        public string FTPHost
        {
            get { return _FTPHost; }
            set { _FTPHost = value; }
        }

 


    }
}
