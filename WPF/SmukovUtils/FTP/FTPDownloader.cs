﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Reflection;

namespace SmukovUtils.FTP
{
    public class FTPDownloader
    {
        #region Properties

        private bool _cancelationPending = false;

        /// <summary>
        /// Used to cancel download from outside.
        /// </summary>
        public bool CancelationPending
        {
            get { return _cancelationPending; }
            set { _cancelationPending = value; }
        }

        #endregion

        #region Delegates and Event Handlers

        public delegate void OnDownloadProgressUpdateEventHandler(int progress);
        public event OnDownloadProgressUpdateEventHandler OnDownloadProgressUpdate;

        #endregion

        #region Constructor

        public FTPDownloader()
        {
            _cancelationPending = false;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Downloads the file from FTP server and reports the progress in % through 
        /// OnDownloadProgressUpdate event
        /// </summary>
        /// <param name="fileNameToDownload">File to download (full FTP path)</param>
        /// <param name="userName">FTP server username</param>
        /// <param name="password">FTP server password</param>
        /// <param name="tempDirPath">Download directory</param>
        /// <returns>TRUE - Download is a success, FALSE - Download fails</returns>
        public bool DownloadFileWithProgressUpdate(string fileNameToDownload,
                       string userName, string password, string tempDirPath)
        {
            int index = fileNameToDownload.LastIndexOf('/');
            string downloadPath = fileNameToDownload.Substring(index + 1);
            downloadPath = tempDirPath + downloadPath;

            bool success = false;

            //get the size of the file
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(fileNameToDownload);
            request.Credentials = new NetworkCredential(userName, password);
            request.Method = WebRequestMethods.Ftp.GetFileSize;
            request.Proxy = null;

            long fileSize; // this is the key for ReportProgress
            using (WebResponse resp = request.GetResponse())
            {
                fileSize = resp.ContentLength;
                resp.Close();
            }


            //get the actual file
            FtpWebRequest req = (FtpWebRequest)WebRequest.Create(fileNameToDownload);
            req.Credentials = new NetworkCredential(userName, password);
            req.Method = WebRequestMethods.Ftp.DownloadFile;
            req.UseBinary = true;
            req.Proxy = null;

            try
            {
                using (var response = (FtpWebResponse)req.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (FileStream fs = new FileStream(downloadPath, FileMode.Create))
                        {

                            int Length = 2048;
                            byte[] buffer = new byte[Length];
                            int bytesRead = stream.Read(buffer, 0, Length);
                            int bytes = 0;
                            int totalSize = (int)(fileSize) / 1000; //KBytes

                            while (bytesRead > 0)
                            {
                                if (CancelationPending == true)
                                {
                                    Console.WriteLine("*** Canceling download ***");
                                    break;
                                }

                                fs.Write(buffer, 0, bytesRead);
                                bytesRead = stream.Read(buffer, 0, Length);
                                bytes += bytesRead;// don't forget to increment bytesRead !

                                this.OnDownloadProgressUpdate((bytes / 10) / totalSize);
                            }
                            fs.Close();
                        }
                        stream.Close();
                    }
                    response.Close();
                }

                this.OnDownloadProgressUpdate(100);
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }



        /// <summary>
        /// Downloads the file from FTP server
        /// </summary>
        /// <param name="fileNameToDownload">File to download (full FTP path)</param>
        /// <param name="userName">FTP server username</param>
        /// <param name="password">FTP server password</param>
        /// <param name="tempDirPath">Download directory</param>
        /// <returns>TRUE - Download is a success, FALSE - Download fails</returns>
        public bool DownloadFile(string fileNameToDownload,
                       string userName, string password, string tempDirPath)
        {
            Console.WriteLine(" **** STARTED DOWNLOADING **** ");
            int index = fileNameToDownload.LastIndexOf('/');
            string downloadPath = fileNameToDownload.Substring(index + 1);
            downloadPath = tempDirPath + downloadPath;
            Console.WriteLine(downloadPath);

            FtpWebRequest req = (FtpWebRequest)FtpWebRequest.Create(fileNameToDownload);
            req.Method = WebRequestMethods.Ftp.DownloadFile;
            req.Credentials = new NetworkCredential(userName, password);
            req.UseBinary = true;
            req.Proxy = null;

            bool success = false;

            try
            {
                using (var response = (FtpWebResponse)req.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (FileStream fs = new FileStream(downloadPath, FileMode.Create))
                        {

                            byte[] buffer = new byte[2048];
                            int ReadCount = stream.Read(buffer, 0, buffer.Length);
                            while (ReadCount > 0)
                            {
                                fs.Write(buffer, 0, ReadCount);
                                ReadCount = stream.Read(buffer, 0, buffer.Length);
                            }
                            fs.Close();
                        }
                        stream.Close();
                    }
                    response.Close();
                }

                success = true;
            }
            catch (Exception)
            {
                success = false;
            }

            Console.WriteLine(" **** FINISHED DOWNLOADING **** ");

            return success;
        }

        /// <summary>
        /// THIS METHOD WASN'T TESTED! Checks the date and time the specified file was last modified on the server.
        /// </summary>
        /// <param name="fileNameToCheck">File to which date we need (full FTP path)</param>
        /// <param name="userName">FTP server username</param>
        /// <param name="password">FTP server password</param>
        /// <returns>File creation time</returns>
        public DateTime? GetFileCreationDate(string fileNameToCheck,
                       string userName, string password)
        {
            // Get the object used to communicate with the server.
            FtpWebRequest req = (FtpWebRequest)WebRequest.Create(fileNameToCheck);
            req.Method = WebRequestMethods.Ftp.GetDateTimestamp;
            req.Credentials = new NetworkCredential(userName, password);
            FtpWebResponse response = (FtpWebResponse)req.GetResponse();
            DateTime lastModified = response.LastModified;

            return lastModified;
        }

        /// <summary>
        /// <para>Call only once to set the settings for the entire Application domain. </para>
        /// <para>It fixes the problem with file download from FTP server, makes it work like in .NET 3.5, since </para>
        /// <para>in .NET 4.0 there were some changes that made it buggy.</para>
        /// 
        /// </summary>
        public static void SetMethodRequiresCWD()
        {
            Type requestType = typeof(FtpWebRequest);
            FieldInfo methodInfoField = requestType.GetField("m_MethodInfo", BindingFlags.NonPublic | BindingFlags.Instance);
            Type methodInfoType = methodInfoField.FieldType;


            FieldInfo knownMethodsField = methodInfoType.GetField("KnownMethodInfo", BindingFlags.Static | BindingFlags.NonPublic);
            Array knownMethodsArray = (Array)knownMethodsField.GetValue(null);

            FieldInfo flagsField = methodInfoType.GetField("Flags", BindingFlags.NonPublic | BindingFlags.Instance);

            int MustChangeWorkingDirectoryToPath = 0x100;
            foreach (object knownMethod in knownMethodsArray)
            {
                int flags = (int)flagsField.GetValue(knownMethod);
                flags |= MustChangeWorkingDirectoryToPath;
                flagsField.SetValue(knownMethod, flags);
            }
        }

        #endregion
    }
}
