﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Reflection;

namespace SmukovUtils.FTP
{
    public class FTPUploader
    {
        //These strings are kept in case that upload fails because 
        //directories aren't created on FTP server
        List<string> directories = new List<string>();
        string rootPath = "";

        /// <summary>
        /// Uploads specified file to server in current users directory
        /// </summary>
        /// <param name="filePath">File to be uploaded</param>
        public string UploadFile(string filePath, string uploadPath, FTPDetails details)
        {
            uploadPath = uploadPath.Replace("\\", "/");
            directories = uploadPath.Split('/').ToList();

            //Get a FileInfo object for the file that will be uploaded
            FileInfo toUpload = new FileInfo(filePath);
            string fileName = toUpload.Name;

            //setup a stream for file that we are going to upload
            FileStream file = File.OpenRead(filePath);

            //string ftpRootPath = "ftp://67.227.215.86/public_ftp/incoming/images/";
            string ftpRootPath = details.RootPath;
            string ftpSavingPath = ftpRootPath;
            rootPath = ftpRootPath;

            foreach (string dir in directories)
            {
                ftpSavingPath += dir + "/";
            }       

            NetworkCredential networkCredentials =
                 new NetworkCredential(details.Username, details.Password);

            //Get a new FtpWebRequest object
            FtpWebRequest request = null;
            //stream for the request
            Stream ftpStream = null;
            try
            {
                try
                {
                    request = (FtpWebRequest)WebRequest.Create(
                        ftpSavingPath + fileName);

                    //Method will be UploadFile
                    request.Method = WebRequestMethods.Ftp.UploadFile;

                    //Set our credentials
                    request.Credentials = networkCredentials;

                    //Setup a stream for the request
                    using (ftpStream = request.GetRequestStream())
                    {

                        //setup variables we'll use to read the file
                        int length = 1024;
                        byte[] buffer = new byte[length];
                        int bytesRead = 0;

                        //Write the file to the request stream
                        do
                        {
                            bytesRead = file.Read(buffer, 0, length);
                            ftpStream.Write(buffer, 0, bytesRead);
                        }
                        while (bytesRead != 0);

                        file.Close();

                        try
                        {
                            ftpStream.Close();
                        }
                        catch (WebException)
                        {
                        }
                    }
                }
                catch (WebException ex)
                {

                    Console.WriteLine("Web Exception 1st - {0} --- {1}", fileName, DateTime.Now.ToString("hh:mm:ss"));

                    Console.WriteLine(ex.Message);
                    Console.WriteLine("******************************");
                    Console.WriteLine(ex.StackTrace);
                    Console.WriteLine("******************************");
                    Console.WriteLine(ftpSavingPath + fileName);

                    CreateServerDirectories(networkCredentials);

                    //Try again to upload
                    try
                    {
                        request = (FtpWebRequest)WebRequest.Create(
                        ftpSavingPath + fileName);

                        //Method will be UploadFile
                        request.Method = WebRequestMethods.Ftp.UploadFile;

                        //Set our credentials
                        request.Credentials = networkCredentials;

                        //Setup a stream for the request
                        using (ftpStream = request.GetRequestStream())
                        {
                            //setup variables we'll use to read the file
                            int length = 1024;
                            byte[] buffer = new byte[length];
                            int bytesRead = 0;

                            //Write the file to the request stream
                            do
                            {
                                bytesRead = file.Read(buffer, 0, length);
                                ftpStream.Write(buffer, 0, bytesRead);
                            }
                            while (bytesRead != 0);

                            file.Close();

                            try
                            {
                                ftpStream.Close();
                            }
                            catch (WebException)
                            {
                            }
                        }

                    }
                    catch (WebException)
                    {
                        //upload failed, don't write to db
                        Console.WriteLine("UPLOAD FAILED - {0} --- {1}", fileName, DateTime.Now.ToString("hh:mm:ss"));
                        file.Close();
                        return "";
                    }
                }

            }
            finally
            {
                if (ftpStream != null)
                    ftpStream.Close();
                if (file != null)
                    file.Close();
                if (request != null)
                    request.Abort();
            }

            Console.WriteLine("File Uploaded - {0} --- {1}", fileName, DateTime.Now.ToString("hh:mm:ss"));

            return ftpSavingPath + fileName;
        }

        /// <summary>
        /// Creates new directories on server
        /// </summary>
        /// <param name="networkCredentials">Network credentials for ftp connection</param>
        private void CreateServerDirectories(NetworkCredential networkCredentials)
        {
            

            //if application directory didn't existed we can use this variable
            //to make other folders with less WebRequest calls because it will assume
            //that other subfolders do not exist too
            bool rootDir = true;

            string dir = rootPath;

            for (int i = 0; i < directories.Count; i++)
            {
                if (directories[i].Trim() == "")
                    continue;

                dir += directories[i] + "/"; 


                //try to create the directory
                try
                {
                    Console.WriteLine("******** Creating Dir " + dir);

                    WebRequest request1 = WebRequest.Create(dir);
                    
                    if(i == 0)
                        request1.Method = WebRequestMethods.Ftp.ListDirectory;
                    else//if root didn't existed then create the new directory immediately
                        request1.Method = rootDir ?
                        WebRequestMethods.Ftp.ListDirectory : WebRequestMethods.Ftp.MakeDirectory;

                    request1.Credentials = networkCredentials;
                    using (var r = request1.GetResponse())
                    {

                    }
                }
                catch (WebException)
                {
                    Console.WriteLine("Web Exception 2nd - CREATING " + dir.ToUpper() + " DIR ");

                    if(i == 0)
                        rootDir = false;

                    WebRequest request1 = WebRequest.Create(dir);
                    request1.Method = WebRequestMethods.Ftp.MakeDirectory;
                    request1.Credentials = networkCredentials;
                    using (var r = request1.GetResponse())
                    {

                    }
                }

            }    
        }


        [Obsolete("Use the one from FTPDownloader class")]
        public bool DownloadFile(string FtpUrl, string fileNameToDownload,
                        string userName, string password, string tempDirPath)
        {
            Console.WriteLine(" **** STARTED DOWNLOADING **** ");
            int index = fileNameToDownload.LastIndexOf('/');
            string downloadPath = fileNameToDownload.Substring(index + 1);
            downloadPath = tempDirPath + downloadPath;
            Console.WriteLine(downloadPath);

            FtpWebRequest req = (FtpWebRequest)FtpWebRequest.Create(fileNameToDownload);
            req.Method = WebRequestMethods.Ftp.DownloadFile;
            req.Credentials = new NetworkCredential(userName, password);
            req.UseBinary = true;
            req.Proxy = null;

            bool success = false;

            try
            {
                using (var response = (FtpWebResponse)req.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (FileStream fs = new FileStream(downloadPath, FileMode.Create))
                        {

                            byte[] buffer = new byte[2048];
                            int ReadCount = stream.Read(buffer, 0, buffer.Length);
                            while (ReadCount > 0)
                            {
                                fs.Write(buffer, 0, ReadCount);
                                ReadCount = stream.Read(buffer, 0, buffer.Length);
                            }
                            fs.Close();
                        }
                        stream.Close();
                    }
                    response.Close();
                }

                success = true;
            }
            catch (Exception)
            {
                success = false;
            }

            Console.WriteLine(" **** FINISHED DOWNLOADING **** ");

            return success;
        }

        /// <summary>
        /// <para>Call only once to set the settings for the entire Application domain. </para>
        /// <para>It fixes the problem with file download from FTP server, makes it work like in .NET 3.5, since </para>
        /// <para>in .NET 4.0 there were some changes that made it buggy.</para>
        /// 
        /// </summary>
        public static void SetMethodRequiresCWD()
        {
            Type requestType = typeof(FtpWebRequest);
            FieldInfo methodInfoField = requestType.GetField("m_MethodInfo", BindingFlags.NonPublic | BindingFlags.Instance);
            Type methodInfoType = methodInfoField.FieldType;


            FieldInfo knownMethodsField = methodInfoType.GetField("KnownMethodInfo", BindingFlags.Static | BindingFlags.NonPublic);
            Array knownMethodsArray = (Array)knownMethodsField.GetValue(null);

            FieldInfo flagsField = methodInfoType.GetField("Flags", BindingFlags.NonPublic | BindingFlags.Instance);

            int MustChangeWorkingDirectoryToPath = 0x100;
            foreach (object knownMethod in knownMethodsArray)
            {
                int flags = (int)flagsField.GetValue(knownMethod);
                flags |= MustChangeWorkingDirectoryToPath;
                flagsField.SetValue(knownMethod, flags);
            }
        }
    
    }
}
