﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmukovUtils.CustomTypes.Queue
{
    public class FixedSizedQueue<T>
    {
        Queue<T> q = new Queue<T>();

        public Queue<T> InnerQueue
        {
            get { return q; }
            set { q = value; }
        }

        public int Limit { get; set; }
        public void Enqueue(T obj)
        {
            q.Enqueue(obj);
            lock (this)
            {
                while (q.Count > Limit)
                    q.Dequeue();
            }
        }
    }
}
