﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;

namespace SmukovUtils.CustomTypes.Queue
{
    class FixedSizedConcurentQueue<T>
    {
        ConcurrentQueue<T> q = new ConcurrentQueue<T>();

        public ConcurrentQueue<T> InnerQueue
        {
            get { return q; }
            set { q = value; }
        }

        public int Limit { get; set; }
        public void Enqueue(T obj)
        {
            q.Enqueue(obj);
            lock (this)
            {
                T overflow;
                while (q.Count > Limit && q.TryDequeue(out overflow)) ;
            }
        }
    }
}
