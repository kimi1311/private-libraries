﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmukovUtils
{
    public class Disposer
    {
        /// <summary>
        /// Used to release COM objects
        /// </summary>
        /// <param name="obj">COM object</param>
        /// <param name="callGC">TRUE - will call GC after it releases the object</param>
        public static void ReleaseCOMObject(object obj, bool callGC)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            finally
            {
                if(callGC)
                    GC.Collect();
            }
        }
    }
}
