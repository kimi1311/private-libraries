﻿using System;
using System.Net.NetworkInformation;
using Timer = System.Threading.Timer;
using System.Net;

namespace SmukovUtils
{

    /// <summary>
    /// Observers for network status change and fires an event if a change occured. 
    /// When this event is fired you can check the availability of the internet connection.
    /// </summary>
    public class NetworkStatusObserver
    {
        public event EventHandler<EventArgs> NetworkChanged;

        private NetworkInterface[] oldInterfaces;
        private Timer timer;

        #region Public Methods

        /// <summary>
        /// Starts the timer for network change checks.
        /// </summary>
        /// <param name="interval">Check interval in miliseconds</param>
        public void Start(int interval = 500)
        {
            timer = new Timer(UpdateNetworkStatus, null, new TimeSpan(0, 0, 0, 0, interval), new TimeSpan(0, 0, 0, 0, interval));

            oldInterfaces = NetworkInterface.GetAllNetworkInterfaces();
        }

        /// <summary>
        /// Checks for internet connection by trying to open www.google.com
        /// </summary>
        /// <returns>TRUE - Internet connection available, FALSE - Otherwise</returns>
        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Private Methods

        private void UpdateNetworkStatus(object o)
        {
            var newInterfaces = NetworkInterface.GetAllNetworkInterfaces();
            bool hasChanges = false;
            if (newInterfaces.Length != oldInterfaces.Length)
            {
                hasChanges = true;
            }
            if (!hasChanges)
            {
                for (int i = 0; i < oldInterfaces.Length; i++)
                {
                    if (oldInterfaces[i].Name != newInterfaces[i].Name || oldInterfaces[i].OperationalStatus != newInterfaces[i].OperationalStatus)
                    {
                        hasChanges = true;
                        break;
                    }
                }
            }

            oldInterfaces = newInterfaces;

            if (hasChanges)
            {
                RaiseNetworkChanged();
            }
        }

        private void RaiseNetworkChanged()
        {
            if (NetworkChanged != null)
            {
                NetworkChanged.Invoke(this, null);
            }
        }

        #endregion

    }

}
